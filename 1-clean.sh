#!/bin/bash
airmon-ng check kill

killall hostapd-wpe;
killall bettercap;

service dnsmasq stop

iptables -F
iptables -X

iptables -t nat -F
iptables -t nat -X

iptables -t mangle -F
iptables -t mangle -X

ip addr flush dev wlan0
