// called when the script is loaded
function onLoad() {

}

// called when the request is received by the proxy
// and before it is sent to the real server.
function onRequest(req, res) {


	if (req.Hostname != "router.asus.com" || req.Hostname != "DOMAIN" || req.Hostname != "www.msftncsi.com"){
		res.Status = 301;
		res.SetHeader("Location","http://router.asus.com/reboot")
	}

	if (req.Hostname == "www.msftncsi.com" ){
		res.Status      = 200;
		res.ContentType = "text/html";
		res.Body        = "Microsoft NCSI";
		headers         = res.Headers.split("\r\n")
		for (var i = 0; i < headers.length; i++) {
			header_name = headers[i].replace(/:.*/, "")
        		res.RemoveHeader(header_name);
		}
		res.SetHeader("Connection", "close");
	}


	if (req.Hostname == "DOMAIN" ){
		res.Status      = 200;
		log_info("!!!RUN KILL COMMAND!!!")
		res.ContentType = "text/html";
		res.Body        = "LOCAL";
		headers         = res.Headers.split("\r\n")
		for (var i = 0; i < headers.length; i++) {
			header_name = headers[i].replace(/:.*/, "")
        		res.RemoveHeader(header_name);
		}
		res.SetHeader("Access-Control-Allow-Origin","*")
		res.SetHeader("Connection", "close");
	}

	if (req.Hostname == "router.asus.com" ){
		res.Status      = 200;
		res.ContentType = "text/html";
		res.Body        = readFile("/opt/Captive/asus-page.html");
		headers         = res.Headers.split("\r\n")
		for (var i = 0; i < headers.length; i++) {
			header_name = headers[i].replace(/:.*/, "")
        		res.RemoveHeader(header_name);
		}
		res.SetHeader("Connection", "close");
	}

}

// called when the request is sent to the real server
// and a response is received
function onResponse(req, res) {

	
}

// called every time an unknown session command is typed,
// proxy modules can optionally handle custom commands this way:
function onCommand(cmd) {
    if( cmd == "test" ) {
        /*
         * Custom session command logic here.
         */

        // tell the session we handled this command
        return true
    }
}
